/**
 * @file displayssd.ino
 * @author José Antonio
 * @brief 
 * @version 0.1
 * @date 2024-05-18
 * 
 * @copyright Copyright (c) 2024
 * Programa control del display SSD1306 de 128x64
 */
#include <Adafruit_SSD1306.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
//------------------------------------------------------------------------------------
#define OLED_RESET -1
Adafruit_SSD1306 display(OLED_RESET);
long contador1 = 0;
bool cambio_limite = false;
byte boton_dwn = 2;
byte boton_up = 3;
long tiempo_anterior_up;
long tiempo_anterior_dwn;
long diferencia_up;
long diferencia_dwn;

//------------------------------------------------------------------------------------
void setup() {
    display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
    display.display();  //inicialización del display SSD1306 mostrando el adafruit
    delay(2000);
    display.clearDisplay();

    //interrupciones
    pinMode(7,INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(boton_up), sube_limite, FALLING);
    pinMode(8,INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(boton_dwn), baja_limite, FALLING);
}
//------------------------------------------------------------------------------------
void loop() {
// función display se llama cada t segundos:
    if (millis() - contador1 > 2000){
        contador1 = millis();
        dibuja_temperatura(23.3);
    }  

    if (cambio_limite){
        cambio_limite = false;
        dibuja_limites(23.3);
        dibuja_curva();
    }
}
//------------------------------------------------------------------------------------
void dibuja_temperatura(float tmp) {
    display.setTextColor(WHITE,BLACK); //color texto, color fondo
    display.setTextSize(1);
    display.setCursor(0, 0);  //posicion izquierda superior
    display.println("T = " + String(tmp,1));  //con un decimal
    display.display();
}
//------------------------------------------------------------------------------------
void dibuja_limites(float tmp){
    display.setTextColor(WHITE,BLACK); //color texto, color fondo
    display.setTextSize(1);
    display.setCursor(64, 0);  //posicion izquierda superior
    display.println("T = " + String(tmp,0));  //con un decimal
    display.display();
}
//------------------------------------------------------------------------------------
void dibuja_cuadricula(){
    //dibujo vertical de pixel
    for (byte y = 13; y < 64; y+=10) {
        //dibuja pixel en horizontal
        for (byte x = 0; x < 128; x+=12) {
            display.drawPixel(x, y, WHITE);
        }
        if (y==43){
            //línea de pixel más próximos entre sí
            for (byte x = 0; x < 128; x+=3) {
                display.drawPixel(x, y, WHITE);
            }
        } 
    }    
}
//------------------------------------------------------------------------------------
void dibuja_curva(){
    for (byte i = 0; i < 128; i++) {
        display.drawPixel(i,32,WHITE);  //dibuja línea horizontal en nivel 32
    }
}
//------------------------------------------------------------------------------------
void sube_limite() {
    diferencia_up = millis() - tiempo_anterior_up;
    tiempo_anterior_up = millis();
    if (diferencia_up > 250){
        cambio_limite = true;
        //tiempo_anterior_up = millis();
    }
}
//------------------------------------------------------------------------------------
void baja_limite() {
    long diferencia_dwn = millis() - tiempo_anterior_dwn;
    tiempo_anterior_dwn = millis();

    if (diferencia_dwn > 250){
        cambio_limite = true;
        tiempo_anterior_dwn = millis();
    }    
}