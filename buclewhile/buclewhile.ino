/**
 * @file buclewhile.ino
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2024-05-05
 * 
 * @copyright Copyright (c) 2024
 * 
 */

void setup() {
  
    Serial.begin(9600);
}

void loop() {
  
    int contador = 0;
    while (contador < 10) {
        Serial.println("Contador ascendente: " + String(contador));
        contador++;
        delay(1000);
        if (contador == 5)
        {
            continue;
        }
        Serial.println("Estoy después del continue");
    }

    contador = 10;
    while (contador >= 0){
        Serial.println("Contador descendente: " + String(contador));
        contador--;
        delay(1000);
        if (contador == 5)
        {
            break;
        }
    }
    
    
}