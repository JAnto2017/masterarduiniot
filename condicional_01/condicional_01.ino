/**
 * Uso de condicionales
 * 
 */

int num_a = 34;
int num_b = 98;

void setup() {
    Serial.begin(9600);
}

void loop() {
    // condicional if
    if (num_a == num_b) {   
        Serial.println("Se ejecuta si la condición es cierta.");
    } else {
        Serial.println("se ejecuta si la condición es falsa.");
    }

    for (int i = 0; i < 5; i++)
    {
        Serial.println("Se ejecuta el número de veces: " + i);
    }
    
}