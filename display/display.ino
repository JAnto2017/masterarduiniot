/**
 * @brief 
 * Utilización del display SSD1306
 */

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET -1
#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2

Adafruit_SSD1306 display(OLED_RESET);

void setup()    {
    Serial.begin(9600);
    display.begin(SSD1306_SWITCHAPVCC, 0X03C);
    display.display();  //inicialización del display SSD1306 mostrando el adafruit
    delay(2000);

    display.clearDisplay();
}

void loop()    {
    display.drawPixel(10,10,WHITE); //coordenada_x,coordenada_y
    display.display();
    delay(1000);

    display.drawPixel(12,10,WHITE); //coordenada_x,coordenada_y
    display.display();
    delay(1000);

    display.drawPixel(14,10,WHITE); //coordenada_x,coordenada_y
    display.display();
    delay(1000);

    //representa un rectángulo x,y,ancho,alto,color
    display.drawRect(0,0,127,63,WHITE); //pantalla completa
    display.display();
    delay(1000);

    //rellena un rectángulo x,y,ancho,alto,color
    display.fillRect(10,10,10,5,WHITE);
    display.display();
    delay(1000);

    //para textos en pantalla
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.setCursor(10,10);
    display.println("Hola mundo");
    display.display();

    display.clearDisplay();
}