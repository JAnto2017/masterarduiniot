/**
 * @file pwm.ino
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2024-05-05
 * 
 * @copyright Copyright (c) 2024
 * Uso de los pines PWM indicados por ~ 
 * Valdrian los pines: 3, 5, 6, 9, 10, 11
 */

const int ledpin = 3;
const int 
void setup(){

}

void loop(){
    for(byte i=0;i<255;i++){
  	    analogWrite(ledpin,i);
        delay(5);
    }

    delay(100);

    for(byte i=255;i>0;i--){
  	    analogWrite(ledpin,i);
        delay(5);
    }
}