
/**
 * @file constantes.ino
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2024-05-05
 * 
 * @copyright Copyright (c) 2024
 * Encender de izquierda a derecha y de derecha a izquierda
 */

const int ledPin13 = 13;
const int ledPin14 = 12;
const int ledPin11 = 11;

void setup() {
    pinMode(ledPin13, OUTPUT);
    pinMode(ledPin14, OUTPUT);
    pinMode(ledPin11, OUTPUT);
}

void loop() {
    for (size_t i = 0; i < 5; i++) {
        on_izquierda_derecha();
    }
    delay(3000);
    for (size_t i = 0; i < 5; i++) {
        on_derecha_izquierda();
    }
}

void on_izquierda_derecha() {
    digitalWrite(ledPin13, HIGH);
    digitalWrite(ledPin14, LOW);
    digitalWrite(ledPin11, LOW);
    delay(1000);
    digitalWrite(ledPin13, LOW);
    digitalWrite(ledPin14, HIGH);
    digitalWrite(ledPin11, LOW);
    delay(1000);
    digitalWrite(ledPin13, LOW);
    digitalWrite(ledPin14, LOW);
    digitalWrite(ledPin11, HIGH);
    delay(1000);
}

void on_derecha_izquierda() {
    digitalWrite(ledPin13, LOW);
    digitalWrite(ledPin14, LOW);
    digitalWrite(ledPin11, HIGH);
    delay(1000);
    digitalWrite(ledPin13, LOW);
    digitalWrite(ledPin14, HIGH);
    digitalWrite(ledPin11, LOW);
    delay(1000);
    digitalWrite(ledPin13, HIGH);
    digitalWrite(ledPin14, LOW);
    digitalWrite(ledPin11, LOW);
    delay(1000);
}