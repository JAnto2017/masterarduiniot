# MASTER ARDUINO IOT

## Añadir los ficheros

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/JAnto2017/masterarduiniot.git
git branch -M main
git push -uf origin main
```

***

- [MASTER ARDUINO IOT](#master-arduino-iot)
  - [Añadir los ficheros](#añadir-los-ficheros)
  - [Avances IoT (MQTT)](#avances-iot-mqtt)
  - [Placa Arduino Uno](#placa-arduino-uno)
  - [Componentes de la placa Arduino Uno](#componentes-de-la-placa-arduino-uno)
  - [Programación C++](#programación-c)
    - [Variables](#variables)
      - [Ámbito de las variables](#ámbito-de-las-variables)
    - [Arrays](#arrays)
    - [Operadores artiméticos](#operadores-artiméticos)
    - [Operadores condicionales](#operadores-condicionales)
    - [Condicionales](#condicionales)
    - [Bucles](#bucles)
      - [Bucle for](#bucle-for)
      - [Bucle while](#bucle-while)
      - [Instrucciónes break, continue](#instrucciónes-break-continue)
    - [Funciones](#funciones)
  - [Prácticas](#prácticas)
    - [Dos pulsadores y un LED](#dos-pulsadores-y-un-led)
    - [Uso de constantes](#uso-de-constantes)
    - [PWM modulación](#pwm-modulación)
    - [Entradas Analógicas](#entradas-analógicas)
    - [Entradas digitales](#entradas-digitales)
    - [Interrupciones por cambio de estado de un pin](#interrupciones-por-cambio-de-estado-de-un-pin)
    - [Sensor de temperatura](#sensor-de-temperatura)
    - [Display SSD1306](#display-ssd1306)
  
***

## Avances IoT (MQTT)

**MQTT** es un protocolo de transporte de mensajería de **publicación** y **subscripción**. Protocolo liviano, eficiente y sencillo, inventado en 1999.

## Placa Arduino Uno

uController => ATmega328.
Están formado por: CPU, Reloj base con una frecuencia, Contador de Programa, EEPROM, RAM, Puertos I/O.

## Componentes de la placa Arduino Uno

Tiene un *XTAL* con una frecuencia de **16 MHz**.

Dispone de interfaz serial *USB* que permite el envío y recepcción de datos hacia el uC. A su vez, el *C.I.* está conectado al puerto serial del uC que corresponde con: **Pin-0** y el **Pin-1**.

Los **LED** utilizados están conectados a: **Vcc**, **Pin-13**, **Tx (Pin-1)**, **Rx (Pin-0)**.

## Programación C++

### Variables

Números enteros **int** son de 2 bytes y permite números desde el *-32.768* hasta el *32.768*. Acepta modificadores tal como **unsigned** números sin signo.

Números decimales **float** son de 4 bytes y permite números desde el *-3.4E+38* hasta el *3.4E+38*.

Números grandes **long** son de 4 bytes y aceptan desde el *-2,147E9* hasta el *2,147E9*.

```
//declaración y asignación

int miVarNumEnteros = 4;
float miVarNumDecimal = 4.9;
long miVarNumGrande = 4000000000;
```

Las variables de tipo *letra* son las **char**, usamos las comillas simples.

Los **String** son objetos de varias letras, usamos las comillas dobles. La primera letra se escribe en mayúsculas.

```
char miVarLetra = 'a';
String miObjFrase = "Es es un objeto no una clase";
```

Un tipo de datos especial, es el **bool** que almacena únicamente valores de verdadero o falso (**true** o **false**). Las palabras reservadas *true* o *false* se escriben en minúsculas.

```
bool miVarBoolean = true;
```

#### Ámbito de las variables

Las variables utilizadas en un programa tienen un alcance dependiendo de dónde se realice su declaración. Además de que se deben declarar antes de su utilización.

Si se realiza dentro de un *procedimiento* esta variable únicamente es visible dentro del propio *procedimiento*, esto es lo que se conoce como **variable local**.

Para declarar una **variable global** que sea visible en todo el programa, se debe declarar al inicio, antes de cualquier *procedimiento* o *función*.

### Arrays

Conjunto de espacios de memoria, que se numeran desde el *cero* hasta el tamaño deseado. Pueden ser de los tipos de datos antes explicados. Para declarar un **array** se utilizan los corchetes.

Ejemplo de un **array** de enteros de cinco espacios numerados desde el cero hasta el cuatro:

```
int miArray [5];
```

Para declarar un **array** y asignar valores podemos realizarlo:

```
int miArray[3] = {1,2,4};
```

Para asignar un valor en una posición concreta de la **array** podemos hacerlo de la siguiente manera: `miArray[2] = 31;`. Asigna el valor treinta y uno en la posición tercera (recuerda que se comineza a numerar desde la cero).

Para determinar el tamaño de un **array**, se utiliza **sizeof()**. Ejemplo:

```
char letras[15];
int tamanio = sizeof(letras);
```

### Operadores artiméticos

| Operador | Descripción |
| -------- | ----------- |
| +        | realiza la suma |
| -        | realiza la substración |
| /        | realiza la división entera |
| %        | obtiene el resto de la divisón |

### Operadores condicionales

| Operador | Descripción |
| -------- | ----------- |
| ==       | comparador de igualdad |
| >=       | comparador mayor e igual a |
| <=       | comparador menor e igual a |
| !=       | comparador distinto entre |
| >        | comparador mayor estricto a |
| <        | comparador menor estricto a |

### Condicionales

Los tipos de *operadores condicionales* se utilizan en los bucles tal como:

```
if ( a == b) {
  //si es cierto se ejecuta este código
} else {
  //si no es cierto se ejecuta este código
}
```

Una variante, en el **else** es añadir otra condición más, ejemplo:

```
if ( a == b) {
  //si es cierto se ejecuta este código
} else if (c >= d) {
  //si no es cierto a==b y además de cumplir c>=d entonces se ejecuta este código
}
```

### Bucles

#### Bucle for

El bucle **for** tiene la estructura `for(inicio ; fin ; incremento)` permite definir un número de repeticiones conocidas. Cuando finaliza el número de ciclos, el puntero contador de programa continúa con la ejecución del código.

```
for (int i = 0; i < count; i++){
    // el código se repite número de veces
}
```

El *incremento* puede ser de más de la unidad. Por ejemplo `for (int i = 0; i < count; i=i+2)`.

#### Bucle while

El bucle **while** realiza una comprobación condicional al inicio y si es cierta se ejecuta el resto del código. En caso, de que la condición sea falsa, no se ejecuta el código interno al bucle.

```
while (/* condición */) {
    /* código del bucle aquí */
}
```

Para salir del *código del bucle* la condición debe ser **false**. Es decir, si en un momento dado es **true** para entrar al bucle, en otro instante, debe cambiar, ya que sino el bucle se estaría ejecutando de manera **infinita**.

```
int count=0;
while (count < 10){
  count++;
  Serial.println("La cuenta al llegar a 9 sale del bucle");
}
```

#### Instrucciónes break, continue

El **break** se utiliza en los bucles para interrumpir la ejecución. Por ejemplo:

```
int a = 0;
while (a <= 10) {
  if (a == 4){
    break;
  }
  Serial.println("cuenta de 0 a 3, cuando llega a 4 sale del bucle");
}
```

El **continue** se utiliza en los bucles para hacer *saltar* la siguiente instrucción a continuación del **continue**.

```
if (contador == 5) {
  continue;
}
  Serial.println("Esta línea de código NO se ejecuta cuando contador==5");
```

### Funciones

Las **funciones** es una parte de código que nos permite reutilizarla cuantas veces precisemos, en el código del programa principal.

La estructura de las **funciones** es de la forma: `valor_devuelto nombre_función (parámetros_recibidos){}`. Ejemplo de función que recibe un parámetro y devuelve el resultado de tipo entero.

```
int sumar(int a, int b) {
  int resultado = a + b;
  Serial.println("Resultado: " + resultado);
  return (resultado);
}
```

Algunos de los valores devueltos por las funciones son:

- void: cuando no devuelve nada.
- int: para devolver un número entero.
- float: para devolver un número decimal.
- double: para devolver número decimal.
- char: para devolver una letra.
- string: para devolver una palabra.

## Prácticas

### Dos pulsadores y un LED

Utilizar dos pulsadores de entrada que al pulsar cualquiera de los dos, pueda encender el LED. Cambiar para que los dos pulsadores estén pulsados para que el LED se pueda encender.

Ejemplo de código con función **or** de los dos pulsadores.

```
void setup()
{
  pinMode(13, OUTPUT);
  pinMode(6,INPUT);
  pinMode(7,INPUT);
}

void loop(){ 
  if(digitalRead(6) || digitalRead(7)) {
   digitalWrite(13,HIGH);
  } else {
   digitalWrite(13,LOW);
  }
}
```

Ejemplo de código con función **and** de los dos pulsadores.

```
void setup()
{
  pinMode(13, OUTPUT);
  pinMode(6,INPUT);
  pinMode(7,INPUT);
}

void loop(){
  
  if(digitalRead(6) && digitalRead(7)) {
   digitalWrite(13,HIGH);
  } else {
   digitalWrite(13,LOW);
  }
}
```

Variante utilizando **variables globales**:

```
int pulsador_a, pulsador_b;

void setup()
{
  pinMode(13, OUTPUT);
  pinMode(6,INPUT);
  pinMode(7,INPUT);
}

void loop(){
  pulsador_a = digitalRead(6);
  pulsador_b = digitalRead(7);
  if(pulsador_a || pulsador_b) {
   digitalWrite(13,HIGH);
  } else {
   digitalWrite(13,LOW);
  }
}
```

![dos pulsadores y led](./dospulsadoresyled.png)

### Uso de constantes

Práctica del uso de las constantes para declarar **constantes**. En la práctica se utilizarán tres diodos LED que se encenderán en forma de secuencial de izquierdas a derecha y posteriormentente de derechas a izquierdas. Usando bucles **for** y **funciones**.

### PWM modulación

Uso de la función **delayMicroseconds**, para activar y desactivar el diodo LED, modificando el ancho del pulso. Ejemplo de código en el que el LED está un tiempo apagado y otro tiempo diferente encendido, dando la sensación que brilla menos.

```
void loop() {
  // ciclo de trabajo = 1/10
  digitalWrite(pinLed,HIGH);
  delayMicroseconds(100);
  digitalWrite(pinLed,HIGH);
  delayMicroseconds(1000);
}
```

Los pines del Arduino, que tienen el símbolo **~**, son salidas para **PWM**. Esto quiere decir que se repiten la señal en *background* hasta que se detengan o cambiemos la frecuencia.

Se usa la función **analogWrite()** para configurar estos pines con la opción **~**. Basta con indicarlo en el **setup()** no siendo necesario indicarlo en el **loop()**.

Tener en cuenta que el **rango** es de **0** a **255**. Si ponemos en el valor de *127* es lo mismo que un ciclo de trabajo del *50%*.

```
void setup() {
  analogWrite(ledPin,127);
}
```

Ejemplo con diferentes valores espaciados un tiempo, configurado en el **setup()**. A medida que se inicial se van ejecutando las diferentes órdenes del código hasta que se queda en la última instrucción:

```
void setup(){
  analogWrite(ledPin,0);
  delay(1000);
  analogWrite(ledPin,50);
  delay(1000);
  analogWrite(ledPin,100);
  delay(1000);
}
```

![Usando pines PWM](./pwm.png)

### Entradas Analógicas

Usando un **potenciómetro** conectamos en el pin de la izquierda GND, en el pin central es la salida de voltaje (se conecta a pines A0 - A5 del Arduino) y en el pin de la derecha Vcc. Entonces a medida, que se mueve el cursor el voltaje obtenido cambia en los márgenes GND y Vcc.

Se utilizan las **entradas analógicas** que en el *Arduino Uno* van desde el pin **A0, A1, A2, A3, A4, A5**.

Para leer una entrada analógica y escribir una salida PWM para un LED, tenemos:

```
void loop(){
  int varanalog = analogRear(A0);
  analogWrite(3,varanalog);
}
```

La función **map** permite mapear desde unos valores en bit a otros valores en voltios. Ejemplo: `int voltios = map(var_leido,0,1023,0,5);`.

La función **mapfloat(int x, float in_min, float in_max, float out_min, float out_max){}** tiene más precisión que la anterior. Ejemplo: `float voltios = map(var_leido,0,1023,0,5);`.

```
void loop(){
  int varanalog = analogRear(A0);
  int val_adap = map(varanalog,0,1023,0,255);
  analogWrite(3,val_adap);
}
```

### Entradas digitales

El pulsador conectado directamente a la entrada digital no es una buena opción. Se debe utilizar un resistor en serie con el pulsador. Conectando en los extremos del circuito serie Vcc y GND. Entre el resistor y el pulsador se conecta el pin lectua. Con este circuito se evita la lectura errónea de los rebotes de la señal.

![resist_pull_up_down](./resist_pull_up_down.png)

El valor de los resistores debe estar entre **1 kOhm** a **15 kOhm**.

- **Pull Up** el resistor está en la parte superior, en conexión con Vcc.
- **Pull Down** el resistor está en parte inferior, en conexión con GND.

Un ejemplo de lectura del pulsador y almacenamiento en una variable **booleana**: `bool bEstadoPulsador = digitalRead(3);`.

Existe una configuración especial de Arduino que permite no añadir la resistencia al botón. Para activar este resistor interno se usa **INPUT_PULLUP**. El resistor tendrá el pin conectado a los 5 Vcc, por lo tanto, al pulsar el botón verás un estado de **LOW**. Ejemplo de configuración: `pinMode(boton, INPUT_PULLUP);

Una opción programada para evitar los rebotes del pulsador:

````
long tiempo_anterior;
long diferencia;
diferencia = millis() - tiempo_anterior;
tiempo_anterior = millis();
if (diferencia > 500) {
  //código aquí lectura pulsador.
}
````

### Interrupciones por cambio de estado de un pin

El *ATMega328* sólo tiene dos pines de manejar interrupciones. Los pines son el 2 y el 3.

- **Pin2**.
- **Pin3**.

La sintaxis es: `attachInterrupt(digitalPinToInterrupt(BOTON), función_llamada_en_int, CHANGE);`.

El valor de **CHANGE** sirve para determinar flancos de subida o bajada en el pulsador. Otra opción, **RISING** sirve para determinar cuando la señal pasa de 0 a 1. Otra opción, **FALLIN** cuando pasa de 1 a 0. Otra es **LOW** se ejecuta cuando la señal tiene nivel bajo.

### Sensor de temperatura

Se utilizará el sensor de temperatura **DS 18620**, dispone de tres pines (1-GND, 2-Data, 3-Vcc) se debe instalar resistor de 4K7 entre Pin2 (Data) y el Pin3 (Vcc).

Buscar e instalar la librería en el IDE Arduino. Instalar la *DallasTemperature* y la *OneWire by Jim Studt*.

Para incluir la librería en el código: `#include <OneWire.h>`.

Utilización de transistor para activar/desactivar cargas controladas desde el Arduino.

![transistor](./transistor.png)

### Display SSD1306

Display de 128 pixels de ancho y de 64 pixels de alto. Utiliza el protocolo **I2C** serial. Este protocolo **I2C** permite conectar varios displays al mismo puerto. Para que los datos no se pierdan, usan un direccionamiento único para cada dispositivo.

Los pines que usa: SDA(1), SCL(2), Vcc(3), GND(4). ¡Ojo! funciona con 3.3 V.

Requiere de la instalación de la librería: `Adafruit SSD1306` y `Adafruit GFX`.

`display.drawPixel(x,y,color);` Imprime un pixel en la pantalla. El color solo puede ser **BLACK** o **WHITE**.<br>
`display.display();` Para que envíe el pixel a la pantalla.<br>
`display.drawRect(x,y,ancho,alto,color)` Para representar un rectángulo.<br>
`display.fillRect(x,y,ancho,alto,color)` Rellena un rectángulo.<br>
`display.drawRoundedRect(x,y,ancho,alto,radio,color)` Rectángulo con esquinas redondeadas.<br>
`display.fillRoundRect(x,y,ancho,alto,radio,color)` Rectángulo relleno de color, con esquinas redondeadas.<br>
`display.drawTriangle(x1,x2,y1,y2,z1,z2,color)` Triángulo con tres vértices y color.<br>
`display.fillTriangle(x1,x2,y1,y2,z1,z2,color)` Triángulo relleno.<br>
`display.setTextColor(color, colorfondo)` Establece el color del texto y el color del fondo, es importante para que no se vea borroso. <br>

Una forma de mostrar una gráfica en tiempo real de datos, por ejemplo, temperatura. En un display, es almacenar los valores en un array. Y a medida que se van añadiendo un nuevo valor al array, se desplaza una posición el contenido, decrementando, es decir, desde el último elemento del array hasta el primero.

`array[0] = array[1]; array[2] = array[3];` <br>

 Esto lo realizaremos en bucle *for* para un array con 128 valores (0 a 127):

 ```
  for (byte i = 1; i < 128; i++) {
    array_tmp[i-1] = array_tmp[i];
  }
  array_tmp[127] = nuevo_valor;
 ```

