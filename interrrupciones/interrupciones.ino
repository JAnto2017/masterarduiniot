/**
 * @file interrupciones.ino
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2024-05-06
 * 
 * @copyright Copyright (c) 2024
 * Programa que atiende las interrupciones en los pines.
 */

const int LED = 5;
const int BOTON = 3;
bool bEstadoPulsador;

void setup(){
    Serial.begin(9600);

    pinMode(LED, OUTPUT);
    pinMode(BOTON, INPUT_PULLUP);

    attachInterrupt(digitalPinToInterrupt(BOTON), pulsador, FALLING);
}

void loop(){

}

/**
 * @brief 
 * Esta función es llamada cuando se produce la interrupción.
 */
void pulsador(){

    long tiempo_anterior, diferencia;
    
    diferencia = millis() - tiempo_anterior;
    tiempo_anterior = millis();

    if (diferencia > 500) {                     //evita los rebotes pulsador 500 ms
        bEstadoPulsador = digitalRead(BOTON);

        if(bEstadoPulsador){
        digitalWrite(LED, HIGH);
        delay(400);
        }else{
        digitalWrite(LED, LOW);
        delay(400);
        }
    }
}