/**
 * Programa ejemplo del uso de variables
*/

int contador = 0;

void setup() {
  Serial.begin(9600);
  delay(100);
  Serial.println("Inicializado el puerto serial");
}

void loop() {
    contador++;
    Serial.println("Contador = " + contador);
    delay(1000);
}