/**
 * @file pulsador.ino
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2024-05-06
 * 
 * @copyright Copyright (c) 2024
 * Utilización de los pulsadores en configuración: PULL UP / PULL DOWN
 */

const int LED = 5;

void setup(){
    Serial.begin(9600);
    pinMode(3,INPUT);
    pinMode(LED,OUTPUT);
}

void loop(){
    bool iEstadoPulsador = digitalRead(3);
    String sMensaje;

    if (iEstadoPulsador)
    {
        sMensaje = "APAGADO";
        digitalWrite(LED,LOW);
        dely(100);                  //evita el rebote
    } else if (!iEstadoPulsador)
    {
        sMensaje = "ENCENDIDO";
        digitalWrite(LED,HIGH);
        delay(100);
    }
    
    Serial.println(iEstadoPulsador);
}