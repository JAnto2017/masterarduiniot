#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS 2

OneWire oneWire(ONE_WIRE_BUS);

DallasTemperature sensors(&oneWire);

const int Boton_UP = 2;
const int Boton_DOWN = 3;

void setup(){
    Serial.begin(9600);
    Serial.println("Iniciamos sensor de temperature");
    sensors.begin();

    pinMode(Boton_UP, INPUT_PULLUP);
    pinMode(Boton_DOWN, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(Boton_UP), sube_limite, FALLING);
    attachInterrupt(digitalPinToInterrupt(Boton_DOWN), baja_limite, FALLING);
}

long contador = 0;
void loop(){
    /**
     * @brief 
     * Es válido para que no esté continuamente leyendo y enviando al Serial.
     * Permite ahorrar tiempo de procesador.
     * millis inicio en 0 ms y cada cierto tiempo no cumple la condición del if.
     */
    if (millis() - contador > 1000){
        contador = millis();
        sensor.requestTemperature();
        Serial.print("Temperatura: ");
        Serial.print(sensor.getTempCByIndex(0));
    }
}

void sube_limite(){
    Serial.println("Subiendo limite");
}

void baja_limite(){
    Serial.println("Bajando limite");
}